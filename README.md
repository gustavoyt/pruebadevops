# PruebaDevOps

## Name
PruebaDevOps

## Description
API sencilla hecha en python con flask alojada en un Server alpine dockerizado, agregando Makefile y crearno un pipeline en gitlab para su deploy. 

## Installation
Clonar el repositorio para su ejecucion

## Usage
Instalar pip para poder instañar el entorno virtual
Instalar y crear un entorno virtual para poder ejecutar de manera local el proyecto.
Activar el entortno virtual e instalar flask
Correr el archivo app.py

Para correrlo desde el container.

Instalar Docker.
Crar la imagen (ya esta creado el dockerfile) con el comando `docker build -t appflask .`
Una vez creada la imagen ejercutar la imagen con `docker run appflask`
Probar en localhost:4000/ o localhost:4000/users

